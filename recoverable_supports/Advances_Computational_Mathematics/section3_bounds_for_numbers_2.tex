\section{Geometrical Interpretation and Number of Recoverable Supports}
In this section, we deal with the geometrical interpretation of Recoverable Supports presented in Section 1 and its implications on their number.
In the end of this section, we further derive a non-trivial, but heuristic upper bound on this number.
As far as we know, this is a new bound.

\begin{Definition}\label{Def:Bounds}
 For $A\in\mathbb{R}^{m\times n}$ the number $\Lambda(A,k)$ is defined as the number of all Recoverable Supports of $A$ with size $k$, i.e.
\[\Lambda(A,k) := |\{(I,s) : (I,s)\mbox{ is a Recoverable Support of }A\mbox{ with size }k\}|.\]
Further let $\Xi(m,n,k)$ be defined as the maximum of $\Lambda$ over all matrices of size $m\times n$, i.e.
\[\Xi(m,n,k) := \max\{\Lambda(A,k) : A\in\mathbb{R}^{m\times n}\}.\]
\end{Definition}%
For some triples $(m,n,k)$, the values for $\Lambda$ and $\Xi$ will be derived in Sections \ref{Se3:Do} and \ref{Se:ValLam}.
Prior, we briefly sketch some basics on \textit{convex polytopes} in the next section.
% Section \ref{Se3:Pr} and give the mentioned geometrical interpreation of Recoverable Supports in Section \ref{Se3:GeoInt}.
% The approached geometrical interpretation established in \cite{Do04} and its relationship to Theorem \ref{Th:GeometricalInterpretL1} will be discussed in Section \ref{Se3:Do}.

\subsection{Preliminaries}\label{Se3:Pr}
Let $x_1,...,x_m\in\mathbb{R}^n$, then its convex hull $P=\mbox{conv}(x_1,...,x_n)$ is called a \textit{polytope}. 
The \textit{dimension} of a polytope is the dimension of its affine hull; a polytope with dimension $d$ is called \textit{$d$-polytope}. 
We call $P$ \textit{centrally-symmetric} if for all $x\in P$ it holds $-x\in P$.
For $\lambda\in\mathbb{R}^n$ and $c\in\mathbb{R}$ we define the hyperplane $H_{\lambda,c}=\{x:\lambda^Tx=c\}$.
Further the intersection $F=H_{\lambda,c}\cap P$ is called a \textit{face} of $P$ if $\lambda^Tx < c$ holds for all $x\notin F$.
A face of $P$ is also a polytope; more general, any intersection of a polytope with an affine subspace is a polytope. 
The set of all $k$-dimensional faces of $P$ is denoted as $\mathcal{F}_k(P)$. 
A centrally symmetric polytope is called $k$-neigborly if any set of $k+1$ vertices of $P$, not including an antipodal pair, spans a face of $P$.

A face $F$ of the hypercube $C^n := [-1,+1]^n$ is uniquely determined by a pair $(I,s)$ consisting of an index set $I\subset\{1,...,n\}$ and $s\in\{-1,+1\}^I$:
with $(I,s)$ choose $\lambda\in\mathbb{R}^n$ through $\lambda_I=s, \lambda_j = 0$ if $j\notin I$.
We see that for any $y\in\mathbb{R}^n$ with $\lambda^Ty > n-|I|$ it holds $y\notin C^n$.
Hence, it holds that $F=H_{\lambda,(n-|I|)}\cap C^n$ is an $|I|$-dimensional face of $C^n$.
For $F\subset C^n$ we note the following equivalence:
\begin{align}
\begin{array}{c}
 F\in\mathcal{F}_k(C^n)\\
\Leftrightarrow\\
\exists!I\subset\{1,...,n\},|I|=n-k\mbox{ }\forall v,w\in F: v_I\in\{-1,1\}^I, v_I = w_I.
\end{array}
 \label{Eq:FaceIndex}
\end{align}
With $I(F)$ we denote the unique subset of $\{1,...,n\}$ determined by $F\in\mathcal{F}_k(C^n)$.
Since the equivalence also holds for subsets $V\subset F$, we also use $I(V)$ to denote the unique subset.
We collect these observations in the next lemma.
\begin{Lemma}\label{Lemma:faceofcube}
 For $F\in\mathcal{F}_k(C^n)$ there exists $\lambda\in\mathbb{R}^n$ such that
\[F = \{x\in C^n:\lambda^Tx = n-k\}\mbox{ and }C^n\backslash F = \{y\in C^n : \lambda^Ty<n-k\}.\]
\end{Lemma}
%\begin{Proof}
%Consider for $I = I(F)$ the vector $\lambda\in\mathbb{R}^n$ with $\lambda_I = v_I$ for all $v\in F$ and $\lambda_j = 0$ if $j\in I^c$. 
%It holds $\lambda\in F$.
%
%If $x\in C^n$ satisfies $\lambda^Tx = n-k$ then $n-k = \lambda^Tx = \sum_{i\in I}\pm1_i x_i$ and since $|I|=n-k$ it holds $x_i = \pm1_i$ and further $x_I = \lambda_I$. On the other hand, if $x\in F$, then $\lambda_I=v_I$ and $\lambda^Tx = |I|$.
%
%If $y\in C^n\backslash F$ and $\lambda^Ty > n-k$ then there exists $i\in I$ with $|y_i|>1$. Further for $y\in C^n$ with $\lambda^Ty<n-k$ there exists $i\in I$ with $|y_i|<1$ which implies $y_I\neq\lambda_I$ and $y\in C^n\backslash F$.
%\end{Proof}
On the basis of Lemma \ref{Lemma:faceofcube}, we identify the relative interior of a face $F$ with $\mbox{relint}(F)=\{x\in F : |x_i|<1, i\notin I(F)\}$.

For an extensive overview in the field of convex polytopes, we refer to the books by Gr\"unbaum \cite{Gr03} or Ziegler \cite{Zi95}.

Finally, we have all tools for proving the geometrical interpreation of Recoverable Supports suggested in Section 1.

\subsection{Geometrical Interpretation of Recoverable Supports}\label{Se3:GeoInt}
With the introduced notation we will prove the following theorem. Note that the results are similar to the interpretation in \cite{Pl07}.
\begin{Theorem}\label{Th:GeometricalInterpretL1}
Let $A\in\mathbb{R}^{m\times n}$ have rank $l$ and let $k\le l$. 
Then the following statements are equivalent:
\begin{enumerate}
\item\label{EquiTh1} There exists a Recoverable Support of $A$ with size $k$.
\item\label{EquiTh2} There exists $F\in\mathcal{F}_{n-k}(C^n)$ such that $\mbox{relint}(F)\cap\rg{A^T}\neq\emptyset$ and $A_{I(F)}^T$ has full rank.
\item\label{EquiTh3} There exists $V\in\mathcal{F}_{l-k}(C^n\cap\rg{A^T})$ and $v\in V$ with $\|v_{I^c}\|_\infty<1$ and $A_{I}$ has full rank for $I:=I(V)$.
\end{enumerate}
\end{Theorem}
\begin{Proof}
First we state for any subset $I\subset\{1,...,n\}$ with $|I|\le l$ that $A_I$ has full rank if and only if $A_I^T$ has full rank.

$\eqref{EquiTh1}\Rightarrow\eqref{EquiTh2}:$ 
Let $(I,s)$ be a Recoverable Support of $A$ with size $k$.
Choose $\lambda\in\mathbb{R}^n$ with $\lambda_I = s$ and $\lambda_j = 0$ for $j\in I^c$ and consider $F := \{x\in C^n : \lambda^Tx = k\}$. 
It holds $F\in\mathcal{F}_{n-k}(C^n)$.
By assumption there is $w\in\mathbb{R}^m$ such that $A^Tw\in\mbox{relint}(F)$, hence  $\mbox{relint}(F)\cap\rg{A^T}\neq\emptyset$.

$\eqref{EquiTh2}\Rightarrow\eqref{EquiTh3}:$ 
Denote $I=I(F)$ and choose $\lambda\in F$ with $\lambda_j = 0$ for $j\notin I$. Then $V=\{y\in P : \lambda^Ty=k\}$ is a face of $P=C^n\cap\rg{A^T}$ and further $V\subset F$.
Hence $I=I(V)$ and there is $v\in V$ with $\|v_{I^c}\|_\infty<1$.
Since $P$ is an $l$-polytope, it follows $V\in\mathcal{F}_{l-k}(P)$.

$\eqref{EquiTh3}\Rightarrow\eqref{EquiTh1}:$ 
Let $I=I(V)$.
There is $w\in\mathbb{R}^m$ such that $A^Tw = v$ and further $\|A_{I^c}^Tw\|_\infty<1$.
Hence, the pair $(I,v_I)$ is a Recoverable Support of $A$ with size $k$.
\end{Proof}

Theorem \ref{Th:GeometricalInterpretL1} partitions solutions of \eqref{l1} into equivalence classes separated into faces of $C^n$ with different dimensions.
For the rest of this section, we will use the notation of each polytope used in Theorem \ref{Th:GeometricalInterpretL1} and $P := C^n\cap\rg{A^T}$.
A first consequence of the latter theorem gives an equivalent expression of Definition \ref{Def:Bounds}: For $A\in\mathbb{R}^{m\times n}$ with rank $l$ and $k\le l$ it is $\Lambda(A,k) = |\mathcal{F}_{l-k}(P)|$.

Further the second statement from Theorem \ref{Th:poset} delivers the following corollary.
\begin{Corollary}\label{Co:GeometricalInterpretL1}
Let $A\in\mathbb{R}^{m\times n}$ have rank $l$. Then the polytope $P = C^n\cap\rg{A^T}$ is $l$-dimensional, centrally-symmetric, and simple, i.e. any vertex of $P$ is adjacenced by $l$ edges.
\end{Corollary}

With Corollary \ref{Co:GeometricalInterpretL1} we can link \textit{Sparse Reconstruction} to simple, centrally-sym\-me\-tric polytopes. 
Further with the two representations of the geometrical interpretation given by Theorem \ref{Th:GeometricalInterpretL1} we can involve the results from the field \textit{(cross-)sections of a hypercube} from Combinatorial Geometry.
This will be done in Section \ref{Se:ValLam} and \ref{Se3:Bounds}.

\subsection{Geometrical Interpretation of Basis Pursuit by Donoho}\label{Se3:Do}
In this subsection we briefly present the geometrical interpretation of Basis Pursuit by Donoho \cite{Do04,Do04b}.

With the cross-polytope $\mathcal{C} = \{x\in\mathbb{R}^n : \|x\|_1\le1\}$ and the projection operator $A\in\mathbb{R}^{m\times n}$, we consider the projected cross-polytope $A\mathcal{C} = \{Ax : x\in\mathcal{C}\}$ and further the following theorem.
\begin{Theorem}\cite[Theorem 1]{Do04}\label{Th:GeometricalInterpretL1Do}
Let $A\in\mathbb{R}^{m\times n}$. These two statements are equivalent:
\begin{itemize}
 \item The polytope $A\mathcal{C}$ has $2n$ vertices and is $k$-neighborly.
 \item Any $k$-sparse vector solves Basis Pursuit uniquely.
\end{itemize}
\end{Theorem}
Theorem \ref{Th:GeometricalInterpretL1Do} connects Sparse Reconstrunction with projected cross-polytopes.
Thus, one can apply results from convex polytopes like the following necessary condition taken from~\cite{Do04} which is based on \cite{MuSh68}.
In the following, the floor function is denoted by $\lfloor\cdot\rfloor$.
\begin{Corollary}\label{Co:NecK}
 Let $A\in\mathbb{R}^{m\times n}$ with $2 < m \le n-2$. If any $k$-sparse vector $x^*$ solves \eqref{l1} then $k\le\lfloor(m+1)/3\rfloor$.
\end{Corollary}

Further in \cite{Do04b} tools from \cite{AfSc92} are used to count the faces of randomly-projected cross-polytopes.
Considering that any preimage of a face of $A\mathcal{C}$ is a face of $\mathcal{C}$ (e.g. see \cite[Theorem 7.10]{Zi95}), the following lemma connects the property of $k$-neigborliness of $A\mathcal{C}$ and $\mathcal{C}$.
We need the term \textit{$k$-simplex} describing a polytope with $k+1$ vertices.
\begin{Lemma}\cite[Lemma 2.1]{Do04b}\label{Le:FacesKNeighbor}
Let $A$ be a projection and $P = A\mathcal{C}$ such that for $k\in\mathbb{N}$ it holds $|\mathcal{F}_i(P)| = |\mathcal{F}_i(\mathcal{C})|$ for $i=1,...,k-1$.
Then any $F\in\mathcal{F}_l(P)$ is an $l$-simplex for $l=0,...,k-1$ and $P$ is $k$-neighborly.
\end{Lemma}

Hence, with Lemma \ref{Le:FacesKNeighbor} one can say rakishly that if we are losing faces through the projection, Basis Pursuit loses the power of reconstructing sparse vectors.
Moreover, there exist explicit functions $\rho_N,\rho_F:(0,1]\rightarrow[0,1]$ (cf. \cite[Section 3]{Do04b}) such that the following theorems hold.
\begin{Theorem}\cite[Theorem 1]{Do04b}\label{Th:PropNeighbStrong}
 Let $\rho<\rho_N(\delta)$ and $A:\mathbb{R}^n\rightarrow\mathbb{R}^m$ a uniformly-distributed random projection with $m\ge\delta n$. 
Then
\[Prob\{|\mathcal{F}_l(\mathcal{C})|=|\mathcal{F}_l(A\mathcal{C})|, l=0,...,\lfloor\rho m\rfloor\}\rightarrow1\mbox{ as }n\rightarrow\infty.\]
\end{Theorem}
% A weaker notion of neighborliness ic contained in Theorem \ref{Th:PropNeighbWeak}.
% There, an approximate equality of face numbers in the proportional-dimensional case is studied.
\begin{Theorem}\cite[Theorem 2]{Do04b}\label{Th:PropNeighbWeak}
Let $m\sim\delta n$ and $A:\mathbb{R}^n\rightarrow\mathbb{R}^m$ be a uniform random projection. 
Then for $k$ with $k/m\sim\rho, \rho<\rho_F(\delta)$ it holds
\[|\mathcal{F}_k(A\mathcal{C})| = |\mathcal{F}_k(\mathcal{C})|(1+o(1)).\] 
\end{Theorem}
\begin{figure}
\begin{center}\includegraphics[width=80mm]{graphics/fig_pt.pdf}\end{center}
\caption{Functions $\rho_N$ (blue) and $\rho_F$ (red) in Theorems \ref{Th:PropNeighbStrong} and \ref{Th:PropNeighbWeak}.}
\label{Fig:PhaseTransition}
\end{figure}
The functions $\rho_N,\rho_F$ are displayed in Figure \ref{Fig:PhaseTransition} and are known in the context of \textit{Phase Transitions}~\cite{DoTa09}.
Theorem \ref{Th:PropNeighbStrong} implies that for large $m$ and $n$ tending to infinity, with high probability any $\lfloor\rho m\rfloor$-sparse vector $x^*$ is recoverable.
Donoho states \cite[Section 1.5]{Do04b} that the result in Theorem \ref{Th:PropNeighbWeak} can be seen ``as a weak kind of neighborliness [...] in which the overwhelming majority of (rather than all) $k$-tuples span $(k-1)$-faces''.
Further he remarks that this result is ``sharp in the sense that for sequences with [$k/m\sim\rho>\rho_F(\delta)$], we do not have the approximate equality''.
An additional result \cite[Theorem 4]{Do04b} is the limit value consideration
\[\lim\limits_{\delta\rightarrow1}\rho_F(\delta) = 1.\]
This value combined with Theorem \ref{Th:PropNeighbWeak} implies that for $\delta\rightarrow1$ and $n\rightarrow\infty$ \textit{almost} all vectors $x^*$ can be recovered through \eqref{l1} since the number $|\mathcal{F}_k(A\mathcal{C})|$ tends to concentrate near its upper bound value $2^{k+1}\binom{n}{k+1}$.

Taking up our geometrical perspective, we introduce the \textit{polar set} $K^*$ of $K\subset\mathbb{R}^m$ as
\[K^* := \{w\in\mathbb{R}^m : x^Tw\le1\mbox{ for all }x\in K\}\]
and see with 
\[A^T(A\mathcal{C})^* = \{A^Tw\in\mathbb{R}^m : |a_i^Tw|\le 1, i=1,...,n\} = \rg{A^T}\cap C^n\]
that the projected cross-polytope $A\mathcal{C}$ and $C^n\cup\rg{A^T}$ are dual to each other, see also \cite{Pl07}, which means that both polytopes have isomorphic face lattices.
Hence, our approach simply differs that we additionally consider unique solutions of Basis Pursuit.

For further considerations, we denote the cross-section of an $m$-dimensional subspace $K$ of $\mathbb{R}^n$ and $C^n$ as \textit{regular} if $K$ has no point in common with any $(n-m-1)$-dimensional face of $C^n$.
The second statement in Theorem \ref{Th:GeometricalInterpretL1} connects regular cross-sections of the hypercube to Recoverable Supports.
In general, we can not assume that the sections occuring through regarding the range of $A^T$ are regular but we still can use some basic result from literature and connect them to \textit{Sparse Reconstruction}.
This is done in Section \ref{Se:ValLam} and \ref{Se3:Bounds}.

\subsection{Values for $\Lambda$}\label{Se:ValLam}
In this subsection, we give some values of $\Lambda$ for specified matrices and sizes of their Recoverable Supports. 
In general, the polytope $P=\rg{A^T}\cap C^n$ is not a regular cross-section.
Thus, the already difficult problem of counting $k$-faces of a (simple) polytope becomes even more difficult counting only all $k$-faces of $P$ intersecting with $(n-m+k)$-faces of $C^n$ in case of full rank matrices. 
Different from $\Xi$ (cf. Section \ref{Se3:Bounds}), using past results for a lower bound of $\Lambda$ over all $m\times n$-matrices is, as far as we know, only possible under certain assumptions, as the following corollary states.
\begin{Corollary}\label{Co:LowerBound}
 Let $A\in\mathbb{R}^{m\times n}$ with rank $l$ and assume $\rg{A^T}\cap C^n$ is a regular cross-section.
Then
\[\Lambda(A,l)\ge2^l.\]
\end{Corollary}
\begin{Proof}
 The result follows from Statement 3 of Theorem \ref{Th:GeometricalInterpretL1} and \cite[Corollary 2]{BaLo82}.
\end{Proof}

With the same assumptions, Euler's relation \cite{Po93,Po99} and Steinitz' characterization for $3$-polytopes \cite{St06} can be applied, but the practicability is limited since for every matrix the regularity of its corresponding cross-section has to be checked.
Considering the cross-section as a simple polytope delivers a different lower bound, which is only dependent on the value $\Lambda(A,1)$.
\begin{Corollary}\label{Co:Co:LowerBound_Simple}
 Let $A\in\mathbb{R}^{m\times n}$ with rank $l$. Then
\[\Lambda(A,l)\ge(l-1)\Lambda(A,1) - (l+1)(l-2).\]
\end{Corollary}
\begin{Proof}
 Combining \cite[Theorem 1]{Ba71} and Corollary \ref{Co:GeometricalInterpretL1} proves the result.
\end{Proof}

Note that Corollary \ref{Co:Co:LowerBound_Simple} provides a lower bound on the number of Recoverable Supports of a matrix if the number of Recoverable Supports of size one is known. However, there are no more than $2n$ possibilities and these can be checked easily for any matrix.

For the rest of this section we consider two types of matrices: \textit{Equiangular tight frames} and \textit{Gaussian matrices}.
The term \textit{equiangular tight frame} will be dwelled on later; a Gaussian matrix means that its entries are independant and standard normally distributed random variables, i.e. having mean zero and variance one.

First we consider Gaussian matrices and regard the work of Lonke in \cite{Lo00}.
With $\mbox{erf}$ we denote the \textit{Gauss Error function} and $\mathcal{E}(Z)$ describes the expected value of $Z$. 
\begin{Corollary}\label{Co:EstRandVertices}
Let $A\in\mathbb{R}^{m\times n}$ be a randomly drawn Gaussian matrix.
Then
\[\mathcal{E}(\Lambda(A,m)) = 2^m\binom{n}{m}\sqrt{\frac{2m}\pi}\int_0^\infty e^{-mt^2/2}\left[\mathrm{erf}\left(\frac t{\sqrt{2}}\right)\right]^{n-m}dt.\]
Further it holds that
\begin{align}
\mathcal{E}(\Lambda(A,m))\ge\binom{n}{n-m}2^n\left(\frac 1\pi\arctan\frac1{\sqrt{m}}\right)^{n-m},\label{Eq:EstRandVertices}
\end{align}
where equality holds for $m = n-1$.
\end{Corollary}

\begin{Proof}
 The result follows from \cite[Proposition 2.2, Proposition 2.5]{Lo00} and the second statement of Theorem \ref{Th:GeometricalInterpretL1}.
\end{Proof}

In Section 5 we will match \eqref{Eq:EstRandVertices} with Monte-Carlo samplings.
Additionally, Lonke delivers an asympotic behavior for sizes $k\neq m$.
\begin{Corollary}
Let $A\in\mathbb{R}^{m\times n}$ be a randomly drawn Gaussian matrix. 
Then for $k\neq m$ it holds that
\[\lim\limits_{n\rightarrow\infty}\mathcal{E}(\Lambda(A,k))(2n)^{-k}k! = 1.\]
\end{Corollary}

\begin{Proof}
 Combining \cite[Corollary 3.4]{Lo00} and the second statement of Theorem \ref{Th:GeometricalInterpretL1} proves the assertion.
\end{Proof}

As Lonke says \cite[Section 3]{Lo00}, the value $\Lambda(A,k)$ ``tends to concentrate near the value [$2^{k}\binom{n}{k}$], which bounds it from above'' (cf. the statement of Donoho \cite{Do04b} as an implication of Theorem \ref{Th:PropNeighbWeak}).

For the rest of this section we regard \textit{equiangular tight frames} $\{a_i\}_{1\le i\le n}$ in $\mathbb{R}^m$, where the vector $a_i$ forms the $i$-th column of the $m\times n$-matrix.
Among other things, these frames have the property that any pair of columns has the same inner product.
In case of minimally redundant matrices, i.e. $m=n-1$, the only equiangular tight frame is (up to rotation) the so-called \textit{Mercedes-Benz frame}, see \cite[Section 3.2]{MaPe09} and  \cite{IsPe06}.
Particularly, Mercedes-Benz frames have an additionally property: 
Each row of such a matrix has the mean value equal to zero, in other words, the kernel is spanned by the vector of all ones.
This property can be used to give the exact number of Maximal Recoverable Supports.
Let $n$ be odd.
Since any $v\in\rg{A^T}$ has the mean value zero, any vertex of $P$ has the same property.
We construct these vertices combinatorically by choosing an index set $J\subset\{1,...,n\}$ with $|J| = (n-1)/2$; there are $\binom{n}{(n-1)/2}$ different possibilities choosing $J$. 
Further there are $(n+1)/2$ different possibilities choosing one $l\in\{1,...,n\}\backslash J$. 
For, say, the Mercedes-Benz frame $A\in\mathbb{R}^{n-1\times n}$ it holds that $v\in\mathbb{R}^n$, with $v_i = 1$ for $i\in J$ and $v_l = 0$ as well as the remaining entries having the value $-1$, is an vertex of $P$. 
Hence 
\begin{align}
  \Lambda(A,n-1) = \left(\frac{n+1}2\right)\binom{n}{\frac{n-1}2}.\label{Eq:BoundMB}
\end{align}
Using the same argument for $n$ even, we get $\Lambda(A,n-1) = 0$ but $\Lambda(A,n-2) = (n/2)\binom{n}{n/2}$.
Keeping in mind that the combinatorical amount increases with a decreasing number of $\pm1$, we can construct any Recoverable Support of $A$ with any size, e.g. for $n$ even it holds 
\[\Lambda(A,n-2) = \left(\frac{n-1}2\right)\left(\frac{n+1}2\right)\binom{n}{\frac{n-1}2}.\]
The theoretical results so far are illustrated in Figure \ref{Fig:TheoResults} by Monte Carlo experiments with Mercedes-Benz frames and randomly drawn Gaussian matrices.
 % compared to the lower bound from Corollary \ref{Co:LowerBound}, the estimated value in \eqref{Eq:EstRandVertices} and the value \eqref{Eq:BoundMB} for Mercedes-Benz frame related to the empirical probability.
One may observe that the empirical results agree with the theorectical statements.

\begin{figure}
\begin{center}\includegraphics[width=120mm]{graphics/newgraphics/Figure3-crop.pdf}\end{center}
\caption{Monte Carlo Sampling versus Theorectical Results in Section \ref{Se:ValLam} and \ref{Se3:Bounds}.
Results from Monte Carlo experiments for Mercedes-Benz frame and Gaussian matrix (displayed as crosses) of the size $(n-1)\times n$.
For any $n\ge4$, one thousand pairs $(I,s)$ with $I\subset\{1,...,n\}, |I|=n-1, s\in\{-1,+1\}^I$ where taken randomly and tested whether $(I,s)$ is a Recoverable Support. 
The y-axis displays the proportion of Recoverable Supports versus all tested pairs.
For Mercedes Benz-frames only results for $n$ odd are displayed.
The formula \eqref{Eq:EstRandVertices} is plotted as a straight line, and formula \eqref{Eq:BoundMB} is displayed as points.
The lower bound from Corollary \ref{Co:LowerBound} is displayed in the dashed line.}
\label{Fig:TheoResults}
\end{figure}


For $n$ even we can also construct a matrix $A\in\mathbb{R}^{n-1\times n}$ similar to the formula \eqref{Eq:BoundMB}, this will be revisited in Section \ref{Se3:Bounds}.
\begin{Lemma}\label{Le:ConstNEven}
 Let $A\in\mathbb{R}^{m\times n}$ then there exists a matrix $B\in\mathbb{R}^{m+1\times n+1}$ such that $\Lambda(B,m+1) = 2\Lambda(A,m)$.
\end{Lemma}

\begin{Proof}
Consider the set $W = \{w\in\mathbb{R}^m : w\mbox{ satisfies }\eqref{Conditions:l1}\mbox{ for some }(I,s)\}$ and for $\alpha\neq 0$ the matrix 
\[B = \left[\begin{array}{cc}A & 0\\0 & \alpha\end{array}\right].\]
Then for any $w\in W$ the elements $w^{(1)}=(w,\alpha^{-1})^T, w^{(2)}=(-w,\alpha^{-1})^T$ satisfy \eqref{Conditions:l1} for $B$.
Hence there are $2\Lambda(A,m)$ Recoverable Supports of $B$ with size $m+1$.
\end{Proof}

Since for $n$ even it holds that
\[\left(n+2-\frac n2\right)\binom{n+2}{\frac n2} = 2\left(n+1-\frac n2\right)\binom{n+1}{\frac n2},\]  
and, by denoting $\lfloor\cdot\rfloor$ as the \textit{Floor function}, we can state matrices $A\in\mathbb{R}^{n-1\times n}$ satisfying
\[\Lambda(A,n-1) = \left(n-\left\lfloor\frac n2\right\rfloor\right)\binom{n}{\lfloor\frac n2\rfloor}.\]
This formula will be important in Corollary \ref{Co:ONeil}.

Up to here, the partial order in the set of all Recoverable Supports of a certain matrix has not been used.
The following lemma enters this subject. 
It will be helpful for bounding $\Lambda$ and $\Xi$ and further gives some characteristics about the actual recoverability which is the number of Recoverable Supports in proportion to the total number of $(n-k)$-faces of $C^n$ (where $k$ is the size of the appropriate Recoverable Support).
\begin{Lemma}\label{Le:RatioIncomOutgoVert}
Let $A\in\mathbb{R}^{m\times n}$, then for any $k\le\mathrm{rank}(A)$ with $\Lambda(A,k)\neq 0$, there exists a positive number $\lambda\le2(n-k+1)$ satisfying
\[\lambda\Lambda(A,k-1) = k\Lambda(A,k).\]
\end{Lemma}
\begin{Proof}
Regarding the lattice of all Recoverable Supports of $A$, Theorem \ref{Th:poset} states that any Recoverable Support with size $k$ is adjacent to $k$ Recoverable Supports with size $k-1$, i.e. the number $k\Lambda(A,k)$ states the number of all adjacences between Recoverable Supports with size $k$ and $k-1$.
Hence, there is a positive number $\lambda$ satisfying the desired equation.
Any Recoverable Support $(I,s)$ with size $k-1$ is adjacent to no more than $2(n-k+1)$ Recoverable Supports with size $k$, since $|I^c| = n - k + 1$ and each new $s_j, j\in I^c$, in a Recoverable Support with size $k$ can adopt both signs: a positve or a negative sign.
Hence, it follows $\lambda\le2(n-k+1)$.
\end{Proof}

The number $\lambda$ from Lemma \ref{Le:RatioIncomOutgoVert} states the averaged number of outgoing adjacences from a Recoverable Support with size $k-1$ to Recoverable Supports with size $k$.
The upper bound for $\lambda$ implies a statement for the probability that an appropriate pair $(I,s)$ is a Recoverable Support.
\begin{Proposition}\label{Pr:Recov}
Let $A\in\mathbb{R}^{m\times n}$, then the mapping 
\begin{align}
 k\mapsto\left[2^k\binom{n}{k}\right]^{-1}\Lambda(A,k)\label{Map:Recov}
\end{align}
is monotonically nonincreasing.
\end{Proposition}

\begin{Proof}
 Assume there is $k\le\mbox{rank}(A)$ satisfying
\[\left[2^{k-1}\binom{n}{k-1}\right]^{-1}\Lambda(A,k-1)>\left[2^k\binom{n}{k}\right]^{-1}\Lambda(A,k).\]
Since there is $\lambda\in\mathbb{R}$ such that $\lambda\Lambda(A,k-1) = k\Lambda(A,k)$, it follows that $\lambda > 2(n-k+1)$, which is a contradiction to Lemma \ref{Le:RatioIncomOutgoVert}.
\end{Proof}

The mapping \eqref{Map:Recov} states the ratio between the actual number of Recoverable Supports of $A$ with size $k$ and the total number of all pairs $(I,s)$ with $I\subset\{1,...,n\}, s\in\{-1,+1\}^I$, previously introduced as the recoverability.
The second proposition aims at an actual number of $\Lambda$ for sparsity $\mbox{rank}(A)-1$ if the number of Maximal Recoverable Supports is known. 
\begin{Proposition}\label{Pr:SparsityK-1}
 Let $A\in\mathbb{R}^{m\times n}$ with rank $l$ and assume $\Lambda(A,l)\neq0$.
Then $\Lambda(A,l-1) = \frac l2\Lambda(A,l)$.
\end{Proposition}

\begin{Proof}
Regarding any Recoverable Support $(I,s)$ with size $l-1$, it holds that the null space of $A_I^T$ is one-dimensional.
Since there exist at least one Recoverable Support with size $l$, we can enlarge it, due to Theorem \ref{Th:poset}, in two different directions.
\end{Proof}

Proposition \ref{Pr:SparsityK-1} states another interesting fact about the number of Maximal Recoverable Support:
Noticing that all values of $\Lambda$ are even due to the symmetry of the underlying polytope, we observe for an odd rank $l$ of a matrix $A$ that $\Lambda(A,l)$ is divisible by four or even a higher even number.

\subsection{Bounds and Values for $\Xi$}\label{Se3:Bounds}
\begin{figure}
 \begin{tikzpicture}[ x = {(-0.5cm,-0.5cm)},
			y = {(0.9659cm,-0.25882cm)},
			z = {(0cm, 1cm)},
			scale = 1.2,
			color = {lightgray}]

\tikzset{facestyle/.style={fill=lightgray, draw=black,opacity=.6, very thin, line join = round}}

% Begin Cube
\begin{scope}[canvas is zy plane at x=0]
	\path[facestyle] (0,0) rectangle (2,2);
\end{scope}

\begin{scope}[canvas is zx plane at y=0]
	\path[facestyle] (0,0) rectangle (2,2);
\end{scope}

\begin{scope}[canvas is zy plane at x=2]
	\path[facestyle] (0,0) rectangle (2,2);
\end{scope}

\begin{scope}[canvas is zx plane at y=2]
	\path[facestyle] (0,0) rectangle (2,2);
\end{scope}
% End Cube

% Begin Surface
\draw[fill=brown,draw=red,opacity=.8,very thin,line join=round]
(2.05,-0.1,0.42) --
(2.05,2.05,0.92) --
(-0.05,2.1,1.55) --
(-0.05,-0.05,1.05) --cycle
;
% End Surface

% 
% Begin Intersection Surface and Cube
\draw[very thin,red,dashed,line join=round]
(0,0,1) -- 
(2,0,0.5) --
 (2,2,1) --
 (0,2,1.5) --cycle
;
% End Intersection
\end{tikzpicture}
\hfill
\begin{tikzpicture}[ x = {(-0.5cm,-0.5cm)},
			y = {(0.9659cm,-0.25882cm)},
			z = {(0cm, 1cm)},
			scale = 1.2,
			color = {lightgray}]

\tikzset{facestyle/.style={fill=lightgray, draw=black,opacity=.6, very thin, line join = round}}

% Begin Cube
\begin{scope}[canvas is zy plane at x=0]
	\path[facestyle] (0,0) rectangle (2,2);
\end{scope}

\begin{scope}[canvas is zx plane at y=0]
	\path[facestyle] (0,0) rectangle (2,2);
\end{scope}

\begin{scope}[canvas is zy plane at x=2]
	\path[facestyle] (0,0) rectangle (2,2);
\end{scope}

\begin{scope}[canvas is zx plane at y=2]
	\path[facestyle] (0,0) rectangle (2,2);
\end{scope}
% End Cube

\draw[very thin,red,dashed,line join=round]
(2,0,0) --
 (2,2,1) --
 (0,2,2) --
 (0,0,1)--cycle;


\draw[fill=brown,draw=red,opacity=.8,very thin,line join=round]
(2,0,0) --
 (2,2,1) --
 (0,2,2) --
 (0,0,1)--cycle
;
\end{tikzpicture}
\hfill
\begin{tikzpicture}[ x = {(-0.5cm,-0.5cm)},
			y = {(0.9659cm,-0.25882cm)},
			z = {(0cm, 1cm)},
			scale = 1.2,
			color = {lightgray}]

\tikzset{facestyle/.style={fill=lightgray, draw=black,opacity=.6, very thin, line join = round}}

% Begin Cube
\begin{scope}[canvas is zy plane at x=0]
	\path[facestyle] (0,0) rectangle (2,2);
\end{scope}

\begin{scope}[canvas is zx plane at y=0]
	\path[facestyle] (0,0) rectangle (2,2);
\end{scope}

\begin{scope}[canvas is zy plane at x=2]
	\path[facestyle] (0,0) rectangle (2,2);
\end{scope}

\begin{scope}[canvas is zx plane at y=2]
	\path[facestyle] (0,0) rectangle (2,2);
\end{scope}
% End Cube


\draw[fill=brown,draw=red,opacity=.8,very thin,line join=round]
(1,0,2) --
(2,0,1) -- 
(2,1,0) --
(1,2,0) --
(0,2,1) --
(0,1,2) -- cycle
;
\end{tikzpicture}
\caption{Examples for Sections $\rg{A^T}\cap C^3$. \textit{Left:} Regular Section; \textit{Center:} Not a Regular Section; \textit{Right:} Regular Secction with Mercedes Benz Frame}
\label{Fi:ExSection}
\end{figure}

In this subsection, we give bounds and values for the largest possible number of Recoverable Supports of all matrices with a certain size, i.e.~$\Xi$ (cf. Definition~\ref{Def:Bounds}).

It is obvious that we can slice the three dimensional cube $C^3$ with a hyperplane in maximal six edges, see Figure~\ref{Fi:ExSection}.
As Figure \ref{Fi:ExSection} prompts it is not possible to slice less than four edges without failing the origin, the graphics in the middle shows that it is possible to touch also vertices of the hypercube. 
Despite Theorem \ref{Th:GeometricalInterpretL1} implies that the results from the field \textit{cross-sections of a hypercube} can be used for our issues, these results often require a regular cross-section while, in general, the section $\rg{A^T}\cap C^n$ is not regular.
In contrast to lower bounds (cf. Section \ref{Se:ValLam}), results for an upper bound can be used, as regarded in the following of this subsection.
Note that McMullens Upper Bound Theorem \cite{M70} can not be used as a typical choice, since it exceeds the trivial bound.

Firstly we give an upper bound for $\Xi$ if $k$ is large.
This result is already known \cite[Corollary 1.3]{Do04} (cf. Corollary \ref{Co:NecK}) in the field of \textit{Sparse Reconstruction}.
\begin{Corollary}
Let $0<m<n-1$. If $k>\frac{m+1}3$ then $\Xi(m,n,k)<2^k\binom{n}{k}$.
\end{Corollary}
\begin{Proof}
 This result follows from \cite{La78,MuSh68} and the second statement in Theorem \ref{Th:GeometricalInterpretL1}.
\end{Proof}

Considering minimally redundant matrices, remind $m=n-1$, we get the following value for Maximal Recoverable Supports.
\begin{Corollary}\label{Co:ONeil}
It holds that
\[\Xi(n-1,n,n-1) = \left(n-\left\lfloor\frac n2\right\rfloor\right)\binom{n}{\lfloor\frac n2\rfloor}.\]
\end{Corollary}%
\begin{Proof}
 Combining \cite{Ne71} and Statement 2 of Theorem \ref{Th:GeometricalInterpretL1} proves the result.
\end{Proof}

In Section \ref{Se:ValLam} we have seen that the Mercedes-Benz frame with an odd number of columns and the construction in Lemma \ref{Le:ConstNEven} reaches this value.
Additionally, with the \textit{mutual coherence} slightly more than half of the values $\Xi(n-1,n,k)$ for variable $k$ are known from the following result.
\begin{Corollary}\label{Co:MuCo}
It holds that
\[\Xi(m,n,k) = 2^k\binom{n}{k}\mbox{ if }k<\frac 12\left(1+\sqrt{\frac{m(n-1)}{n-m}}\right).\]
\end{Corollary}%
\begin{Proof}
  This follows from \cite{DoHu01,StHe03}.
\end{Proof}

The bound in Corollary \ref{Co:MuCo} can be reached by equiangular tight frames, see \cite{StHe03}.
As a further consequence of the bound in Lemma \ref{Le:RatioIncomOutgoVert}, the following proposition delivers an upper bound for $\Xi$.

\begin{Proposition}
 For $k\le m$ it holds that
\[\Xi(m,n,k)\le\frac {2(n-k+1)}k\Xi(m,n,k-1).\]
\end{Proposition}

\begin{Proof}
 Assume there is $k\le m$ for $A\in\mathbb{R}^{m\times n}$ with $\Xi(m,n,k-1)=\Lambda(A,k-1)$ and $\tilde A\in\mathbb{R}^{m\times n}$ with $\Xi(m,n,k)=\Lambda(\tilde A,k)$ satisfying
\[\Lambda(\tilde A,k) > \frac {2(n-k+1)}k\Lambda(A,k-1),\]
then it holds that
\[\frac {2(n-k+1)}k\Lambda(A,k-1)<\Lambda(\tilde A,k)\le\frac {2(n-k+1)}k\Lambda(\tilde A,k-1)\]
with Lemma \ref{Le:RatioIncomOutgoVert}, which is a contradiction to $\Xi(m,n,k-1)=\Lambda(A,k)$.
\end{Proof}

Similarly to the value $\Lambda$, the latter result implies further statements about $\Xi$, which are similar to Propositions \ref{Pr:Recov} and \ref{Pr:SparsityK-1}.
\begin{Corollary}\label{Co:Xil-1} It holds that $\Xi(m,n,m-1) = \frac m2\Xi(m,n,m)$.
\end{Corollary}

Additionally, we get a similar statement to Proposition \ref{Pr:Recov} about an upper bound of the recoverability.
\begin{Corollary}\label{Co:RecCurve_Xi}
The mapping
\[k\mapsto\left[2^k\binom{n}{k}\right]^{-1}\Xi(m,n,k)\]
is monotonically nonincreasing.
\end{Corollary}

To the end of this section, we develop a heuristic upper bound of $\Xi$.
Considering $\lambda$ in Lemma~\ref{Le:RatioIncomOutgoVert}, we can establish an upper bound of $\Xi$ by assuming that $\lambda$ can be bounded from below, i.e. $\lambda\ge2(l-k+1)$ for matrices with rank $l$.
Conveniently, we derive this heuristic bound for full rank, minimally redundant matrices $A$, i.e. $l=n-1$, but the construction can be adapted straightforward to other instances.
Assume $\lambda\ge2(n-k)$, then for a positive integer $v < n$ it follows
\[\Lambda(A,n-1) \ge 2^{v-1}\frac{(v-1)!(n-v)!}{(n-1)!}\Lambda(A,n-v)\]
by applying the lower bound recursively.
Through substituting $k= n-v$ and bounding $\Lambda(A,k-1)$ by Corollary~\ref{Co:ONeil}, we obtain
\[\Lambda(A,k) \le 2^{k+1-n}\binom{n-1}k\left(n-\left\lfloor\frac n2\right\rfloor\right)\binom n{\left\lfloor\frac n2\right\rfloor}.\]
Since the right-hand side of the latter inequality exceeds the trivial bound $2^k\binom nk$ for small $k$, we postulate the following heuristic upper bound:
\begin{align}
\Xi(n-1,n,k)\le\min\left\{2^k\binom{n}{k}, 2^{k+1-n}\binom{n-1}{k}\left(n-\left\lfloor\frac n2\right\rfloor\right)\binom{n}{\left\lfloor\frac n2\right\rfloor}\right\}.\label{InEq:UpBoundBeta}
\end{align}
In general, the inequality $\lambda\ge2(l-k+1)$ is not true, but we motivate this bound by the observation that the transition from all pairs $(I,s)$ are Recoverable Supports to none of the pairs $(I,s)$ are Recoverable Supports is rapid, e.g. \cite{TsDo06,BrDoEl09}, and, furthermore, this bound is true and strict in case that $k=l$, cf. Corollary \ref{Co:Xil-1}.
As far as we know, there is no matrix exceeding this heuristic; it will be considered in the computational experiments in Section 5.
Moreover, this bound is also strict due to Corollary~\ref{Co:ONeil}, \ref{Co:MuCo} and \ref{Co:Xil-1} for some values of $k$.

In the context of the Hasse Diagram of all Recoverable Supports, the maximum $\Xi$ also states a geometrical question: what is the maximal $\lambda$ such that the ratio $\lambda/k$ between the outgoing edges of all Recoverable Supports with size $k-1$ and the incoming edges of all Recoverable Supports with size $k$?
Results for this question would give further insights about $\Xi$ and improve a non-trivial upper bound.
Combining Corollary~\ref{Co:RecCurve_Xi} with Corollary \ref{Co:MuCo} delivers an interesting insight for $A\in\mathbb{R}^{m\times n}$: if $\Lambda(A,\tilde k) = 2^k\binom{n}{\tilde k}$ holds for some $\tilde k\le m$, then equality holds in Lemma~\ref{Le:RatioIncomOutgoVert} for all $k\le\tilde k$, and also for $\Xi$.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "main"
%%% End: 
