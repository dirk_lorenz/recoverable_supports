\section{Existence of and Conditions for Recoverable Supports}
% In the following subsections we establish a partial order on the set of all Recoverable Supports of a given matrix (Section \ref{Se2:Poset}), and give a characteristic that determines if there exists a Recoverable Support of a given matrix (Section \ref{Se2:Ex}).
% This characteristic is so native that it seems only malicious constructed matrices are left behind from the utility of \textit{Sparse Reconstruction}. 
% The necessary and sufficient condition in Section \ref{Se2:Con} is significant for identifying this characteristic.
% Before we establish a partial order and expose maximal elements on the set of all Recoverable Supports of a given matrix in the sense of Zorn's Lemma, labeled as \textit{Maximal Recoverable Supports}.
% The achievements in this section will be used for computing a Recoverable Support in Section 4.

\subsection{Establishing a Partial Order}\label{Se2:Poset}
The condition \eqref{Conditions:l1} for Recoverable Supports rests on two things: The injectivity of the submatrix $A_I$ with $I$ being the support of $x^*$ and the existence of the dual certificate $w\in\mathbb{R}^m$.
The following theorem shows that it is possible to shrink Recoverable Supports and states conditions when it is possible to obtain a larger Recoverable Support from a given one.
\begin{Theorem}\label{Th:poset}
 Let $A\in\mathbb{R}^{m\times n}$ and let $S_1=(I,s)$ be a Recoverable Support of $A$.
\begin{enumerate}
\item If for $w$ satisfying \eqref{Conditions:l1} there is $z\in\ker{A_I^T}$ satisfying $\|A_{I^c}^T(w+z)\|_\infty=1$ and $A$ restricted to $J := \{i : |a_i^T(w+z)| = 1\}$ has full rank, then with $t = A_J^T(w+z)$ the pair $S_2=(J,t)$ is a Recoverable Support of $A$ and it holds $I\subset J$.
\item Let $|I|>1$. For any $j_0\in I$ there exists $\tilde{s}\in\{-1,1\}^{I\backslash\{j_0\}}$ with $s_i=\tilde{s}_i$ for all $i\in I\backslash\{j_0\}$, such that the pair $S_3 = (I\backslash\{j_0\},\tilde{s})$ is a Recoverable Support of $A$.
\end{enumerate}
\end{Theorem}
\begin{Proof}
The existence of $z\in\ke{A_I^T}$ for the first statement is obvious and the conclusion that $(J,t)$ is a Recoverable Support follows directly by checking~\eqref{Conditions:l1}. 
For the second statement notice that $A_{I\backslash\{j_0\}}$ has full rank too and that $\ke{A_I^T}\subsetneqq\ke{A_{I\backslash\{j_0\}}^T}$ holds.
Hence, for $w\in\mathbb{R}^m$ satisfying \eqref{Conditions:l1} there exists $z\in\kerZ{A_{I\backslash\{j_0\}}}$ with $a_{j_0}^Tz \neq 0$.
Choose $\lambda\neq0$ such that $|\lambda|<(1-|a_i^Tw|)/|a_i^Tz|$ for all $i\in I^c$ with $a_i^Tz\neq0$ and
\[\lambda a_{j_0}^Tz\in\left\{\begin{array}{cl}(-2,0)&,\mbox{ if }a_{j_0}^Tw = 1\\(0,2)&,\mbox{ else}\end{array}\right.\]
holds. 
Considering all elements of $A^T(w+\lambda z)$ seperately, it follows
\begin{align*}
|a_{j_0}^Tw + \lambda a_{j_0}^Tz| < 1&,\\
a_i^Tw + \lambda a_i^Tz = a_i^Tw = s_i &\mbox{ for }i\in I\backslash\{j_0\},\\
|a_j^Tw + \lambda a_j^Tz| \le |a_j^Tw| + |\lambda||a_j^Tz| < 1&\mbox{ for }j\in I^c
\end{align*}
by construction. 
Hence with $\tilde{s} = A_{I\backslash\{j_0\}}^T(w+\lambda z)$ the pair $S_2 = (I\backslash\{j_0\},\tilde{s})$ is a Recoverable Support of $A$.
\end{Proof}

The following corollary can be obtained by applying the second statement in Theorem \ref{Th:poset} recursively.
\begin{Corollary}\label{Co:maxre}
 Let $A\in\mathbb{R}^{m\times n}$ and $(I,s)$ be a Recoverable Support of $A$. 
Then for any $J\subset I,J\neq\emptyset$, there exists $\tilde{s}\in\mathbb{R}^n$ with $\tilde{s}_J=s_J$, such that the pair $(J,s)$ is a Recoverable Support of $A$.
\end{Corollary}

By using the stated inclusion of Recoverable Supports, a partial order can be obtained through Theorem \ref{Th:poset}:
for Recoverable Supports $S_1 = (I,s), S_2 = (J,\tilde{s})$ with $s_J = \tilde{s}_J$, it is $S_2\le S_1$ if and only if $J\subset I$.
For example, the supports $S_1$, $S_2$ and $S_3$  from Theorem \ref{Th:poset} fulfill $S_3\le S_1\le S_2$.
Moreover, any Recoverable Support can be shrinked and enlarged under the assumption that the respective submatrix is injective.
In other words, the set of all Recoverable Supports form a partially ordered set and may be visualized as a Hasse Diagram.
Further, there exist Recoverable Supports which can not be enlarged, and we call them \emph{Maximal Recoverable Supports}.
Due to Corollary~\ref{Co:maxre}, the Maximal Recoverable Supports determine the full set of all Recoverable Supports.
%In Section 3 we will incorporate Theorem \ref{Th:poset} to establish bounds of the number of Recoverable Supports.

The proof of Theorem \ref{Th:poset} also provides a way to obtain a Recoverable Support if a pair $(I,s)$ satisfies all requirements but having $A_I$ as a full rank matrix. 
\begin{Corollary}
 Let $A\in\mathbb{R}^{m\times n}, I\subset\{1,...,n\}$ and $s\in\{-1,1\}^I$. 
Further let there exist $w\in\mathbb{R}^m$ satisfying $A_I^Tw = s, \|A_{I^c}^Tw\|_\infty<1$. 
If there exists $J\subset I$ such that the submatrix $A_J$ has full rank, then there exists $\tilde{s}\in\{-1,1\}^J$ with $\tilde{s}_J = s_J$ such that $(J,\tilde{s})$ is a Recoverable Support of $A$.
\end{Corollary}

\subsection{Sufficient and Necessary Condition}\label{Se2:Con}
Similar to Section \ref{Se2:Poset}, we will consider dual certificates to establish a sufficient and necessary condition for a pair $(I,s)$ being a Recoverable Support of a given matrix.
For this purpose, we introduce the pseudo-inverse $(A_I^T)^\dagger$ of $A_I^T$.
The following theorem und its corollary are an extension of Fuchs' condition in \cite{Fu04}.
\begin{Theorem}\label{Th:SuffNessCond}
Let $A\in\mathbb{R}^{m\times n}, I\subset\{1,...,n\}$ and $s\in\{-1,1\}^I$. 
Then $(I,s)$ is a Recoverable Support of $A$ if and only if $A_I$ has full rank and there exists $y\in\ker{(A_I^T)}$ such that
\begin{align*}
\|A_{I^c}^T(A_I^{T})^\dagger s+ A_{I^c}^Ty\|_\infty <1.
\end{align*}
\end{Theorem}
\begin{Proof}
If $(I,s)$ is a Recoverable Support, then $A_I$ has full rank and there exists $w\in\mathbb{R}^m$ such that $A_I^Tw=s$. 
With $\tilde{y}\in\mbox{ker}(A_I^T)$ the vector $w$ has the general representation $w = (A_I^{T})^\dagger s+\tilde{y}$.
Since there exists at least one $w$ satisfying $\|A_{I^c}^Tw\|_\infty<1$, there exists $y\in\mbox{ker}(A_I^T)$ proving the stated inequality.

Further, for $y\in\mbox{ker}(A_I^T)$ consider $w= (A_I^{T})^\dagger s + y$. 
Since $A_I$ has full rank, $A_I^T$ has linearly independent rows, so $A_I^Tw = s$ holds as well as $\|A_{I^c}^Tw\|_\infty<1$.
\end{Proof}

Note that a conclusion of Theorem \ref{Th:SuffNessCond} is that
\[A_I\mbox{ has full rank and }\|A_{I^c}^T(A_I^{T})^\dagger s\|_\infty <1\]
is a sufficient condition for $(I,s)$ being a Recoverable Support of $A$ by choosing $y=0$. 
For full rank matrices with $|I|=\mbox{rank}(A)$ this is also a necessary condition using the inverse $A_I^{-T}$ of $A_I^T$.
\begin{Corollary}\label{Co:SuffNessCondFullRank}
Let $A\in\mathbb{R}^{m\times n}$ have a full rank, $I\subset\{1,...,n\}$ with $|I| = m$ and $s\in\{-1,1\}^I$. 
Then $A_I$ is invertible and $\|A_{I^c}^TA_I^{-T}s\|_\infty <1$ holds if and only if $(I,s)$ is a Maximal Recoverable Support of $A$.
\end{Corollary}

% Theorem \ref{Th:SuffNessCond} will be applied in the coming subsection.
% Necessary to that end is that the pseudo-inverse of a vector $x$ is interpreted as $\|x\|^{-2}x^T$.

%\subsection{Existence of Recoverable Supports}\label{Se2:Ex}
We close this section with the characterization on Recoverable Supprts with size one when the corresponding column has the largest norm over all columns of the considered matrix.
The following theorem will be considered in Algorithm~\ref{Algo:ComputingRS}.
\begin{Theorem}\label{Th:Existence}
Let $A\in\mathbb{R}^{m\times n}$ and $k\in\{1,...,n\}$ such that for all $j\neq k$ holds $\|a_j\|\le\|a_k\|$. 
Then for $s\in\{-1,+1\}$ the pair $(\{k\},s)$ is a Recoverable Support of $A$ if and only if for any $j\neq k$ with $\|a_j\|=\|a_k\|$ it holds that $a_j\neq a_k$.
\end{Theorem}

\begin{Proof}
Let $(\{k\},s)$ be a Recoverable Support of $A$ and without loss of generality let $s=+1$. 
Assuming for $j\neq k$ it holds that $a_k=a_j$, then for all $y\bot a_k$ it follows
\[\left|\|a_k\|^{-2}a_j^Ta_k+a_j^Ty\right|=1\]
which is a contradiction to Theorem \ref{Th:SuffNessCond}.

For the converse implication let $a_j\neq a_k$ with $\|a_j\| = \|a_k\|$. 
With $w = \|a_k\|^{-2}a_k$ it holds that
\[|a_k^Tw| = 1\mbox{ and }|a_j^Tw| = \frac{|a_j^Ta_k|}{\|a_k\|^2} < \frac{\|a_j\|}{\|a_k\|} = 1\]
by applying Cauchy-Schwartz inequality.
Further for any $a_i$ satisfying $\|a_i\|<\|a_k\|$ the inequality $|a_i^Tw|<1$ holds.
Trivially, the submatrix $A_{\{k\}}$ has full rank and with $s = a_k^Tw$ it holds that the pair $(\{k\},s)$ is a Recoverable Support of $A$.
\end{Proof}

Hence, every matrix whose columns have the largest norm possesses a Recoverable Support if and only if one of these columns do not appear multiple times.
That does not exclude matrices whose columns with largest norm have multiple appearence from having Recoverable Supports; see for example the matrix $A\in\mathbb{R}^{2\times 4}$ whose first two columns are standard basis elements of $\mathbb{R}^2$ and $a_3=a_4 = a_1+a_2$ has still four Recoverable Supports with size one. 
Moreover, Theorem \ref{Th:Existence} will be useful as a starting point for the algorithm in Section 4.
%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "main"
%%% End: 
