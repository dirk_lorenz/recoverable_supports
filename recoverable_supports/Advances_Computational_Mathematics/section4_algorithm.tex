\section{Computing a Recoverable Support}
\label{sec:comp-rec-supp}
In general, generating test instances for computational experiments is an expensive problem in Basis Pursuit. 
Even for, say, Gaussian matrices, where one only has to find an instance satisfying the optimality condition for $\ell_1$ minimization derived by its subdifferential, it is not straightforward to find a suitable $x^*$ satisfying \eqref{l1} if the desired $x^*$ shall not be very sparse.

One na\"ive way to generate a test instance is to choose an arbitrary $k$-sparse vector $x^*$, solve~(\ref{l1}) with some solver and then check whether the solution is equal to $x^*$.
This may work well for small $k$ but usually becomes computationally expensive for larger $k$. Moreover, this construction suffers from a ``trusted method bias'', i.e. the method used to solve~(\ref{l1}) may work better on instances which inherit a particular structure (something which may not be under control of the experimenter).
Another approach has been proposed in~\cite{Lo12}: Choose a pair $(I,s)$ randomly and construct a dual certificate, i.e. find $w$ as in~(\ref{Conditions:l1}).
This problem could be seen as a convex feasibility problem~\cite{bauschke1996convexfeasibility} and can be solved, e.g., by alternating projections as outlined in~\cite{Lo12}.
This approach often leads to dual certificates $w$ such that the value $\|A_{I^c}^Tw\|_\infty$ is close to one and hence, the result may not be trustworthy due to numerical errors. A more favorable way to check the reconstructability using~(\ref{Conditions:l1}) would be to check if for some $(I,s)$ the optimal value of 
\begin{align}
 \min_w\|A_{I^c}^Tw\|_\infty \mbox{ subject to }A_I^Tw=s_I\label{MinProg:DualCert}
\end{align}
is less or equal one. Similar to the $\ell_1$ minimization problem~(\ref{l1}), this may be cast as a linear program. However, there are important differences to the na\"ive approach: First, the number of variables is $m$ which may be much smaller than $n$. Moreover, one does not rely on the entries of $x^*$ but only on its sign and the support.

However, in all the above methods one generates some trial support $(I,s)$ and then checks whether it is recoverable.
Derived from Corollary \ref{Co:EstRandVertices}, the probability for an appropriate pair $(I,s), |I|=n-1,$ being a Maximal Recoverable Support of a randomly drawn Gaussian matrix of the size $(n-1)\times n$ tends to zero for huge $n$.
Hence, one may never find any $(n-1)$-sparse vector by any trial-and-error method and a similar conclusion is true for $k$-sparse vectors for $m\times n$ matrices if $k$ is sufficiently large.
But in view of Theorem~\ref{Th:poset}, there is a systematic way to generate Recoverable Supports $(I,s)$ with maximal size by selecting a $1$-sparse recoverable vector, computing a corresponding dual certificate and incrementally increasing the support while maintaining a valid dual certificate (according to Theorem~\ref{Th:poset}, 1.).
The method is outlined in Algorithm \ref{Algo:ComputingRS}.
Note that there is considerable freedom in lines \ref{Algo1:ChooseBasisVector} and \ref{Algo1:MinProb} of the algorithm on how to continue.

\begin{algorithm}[htb]
\DontPrintSemicolon
\SetAlgoLined
\SetKwInOut{Input}{Input}\SetKwInOut{Output}{Output}
\SetKwData{Jj}{$|J|\le k$ and $A_J$ has full rank}
 \Input{$A\in\mathbb{R}^{m\times n}, k\le\mbox{rank}(A)$}
 \Output{Recoverable Support $(I,s)$ of $A$ with size $k$}
 \BlankLine
 \emph{$a_k = \arg\max_{a_i}\|a_i\|_2^2$}\tcp*[f]{The $i$-th column of $A$ is denoted by $a_i$}\;
 \emph{$w\leftarrow\|a_k\|_2^{-2}a_k$}\;
 \emph{$s\leftarrow A^Tw$}\label{Algo1:ReSuSize1}\;
 \emph{$I\leftarrow\{k\}$}\label{Algo1:ReSuSize1a}\;
 \emph{$I^c\leftarrow\{1,...,n\}\backslash\{k\}$}\label{Algo1:ReSuSize1b}\;
 \While{$|I|<k$}{\label{Algo1:WhileLoop}
   \emph{Choose a vector $y\in\ker{A_I^T}$}\label{Algo1:ChooseBasisVector}\;

   \emph{Choose $\lambda\in\mathbb{R}$ such that $\|A_{I^c}^T(w+\lambda y)\|_\infty = 1$}\label{Algo1:MinProb}\;
   \emph{$J\leftarrow\{i : |a_i^T(w+\lambda z)| = 1\}$}\;
   \uIf{\Jj}{\label{Algo1:IfCause}
     \emph{$I\leftarrow J$}\;
     \emph{$I^c\leftarrow\{1,...,n\}\backslash I$}\;
     \emph{$w\leftarrow w+\lambda y$}\;
     \emph{$s\leftarrow A^Tw$}\label{Algo1:WalkTheSubset}\;
   }
   \lElse{
     \emph{Return to line \ref{Algo1:ChooseBasisVector}}\label{Algo1:ReturnToChooseBasisVector}
   }
 }
 \caption{Computing a Recoverable Support}\label{Algo:ComputingRS}
\end{algorithm}

Algorithm \ref{Algo:ComputingRS} is designed for arbitrary matrices of arbitrary sizes.
However, it is possible that the algorithm does not deliver a desired Recoverable Support if it gets stuck in line~\ref{Algo1:IfCause}.
To protect against these cases the method could be extended by including the second statement of Theorem \ref{Th:poset};
this extension would deliver more freedom to jump between different index sets $I$ but requires elaborate bookkeeping of previously visited index sets.
We experienced that this extension is not necessary in most cases.

The first issue about the algorithm might be the question, for what kind of matrices does the method compute a Recoverable Support. 
Theorem \ref{Th:Existence} gives an answer: matrices whose columns with maximal Euclidean norm are pairwise linearly independant.
The construction in the proof of Theorem \ref{Th:Existence} for a Recoverable Support with size one is used in the first three lines.
Hence, for these  matrices the variable $s$ in line \ref{Algo1:ReSuSize1} has only one entry equal to one in absolute value, the rest of the absolute entries are less than one; this occasions the clauses in line \ref{Algo1:ReSuSize1a} and \ref{Algo1:ReSuSize1b}.
%In general, an extension to other startings is not necessary since Theorem \ref{Th:Existence} covers a huge number of types of matrices.

Theorem \ref{Th:GeometricalInterpretL1} gives a geometrical interpretation of Algorithm \ref{Algo:ComputingRS}. 
In line \ref{Algo1:ReSuSize1} we start on one facet of the hypercube and by line \ref{Algo1:WalkTheSubset} we walk along the range of the transposed matrix to the next lower-dimensional face of the hypercube.
Consequently, the method requires at least $k-1$ iterations for computing a Recoverable Support with size $k$.
Experiences show that mostly only $k-1$ iterations are required.
The if-clause in line \ref{Algo1:IfCause} saves for being \textit{stuck} in an unsuitable face.

In any iteration step of the while loop, an element of the corresponding null space is chosen.
To choose such a vector it is advantageous to maintain an orthonormal basis for the kernel of $A_I^T$ during the iteration in the form of some decomposition.
In our setting, we are calling up a rank one update to a QR decomposition.
In the worst-case scenario it may happen that one needs to check several vectors $y$ in line~\ref{Algo1:ChooseBasisVector}, however, using an orthonormal basis of the kernel one can just try all of the basis vectors one after another.
This worst case would lead to an iteration number $\mathcal{O}(l^2)$ for computing a Recoverable Support with size $l$.
Actually, we were not able to construct such an instance and usually the iteration number is $\mathcal{O}(l)$.
Our setting of this method, implemented as a MATLAB program, can be found online at \homepage.
Further, experiments with Gaussian matrices and Mercedes-Benz frames are evaluated in one of the author's PhD thesis \cite{Kr15}.

% For many Recoverbable Support $(I,s)$ there exists many elements $w$ satisfying \eqref{Conditions:l1}, hence one can also extend Algorithm \ref{Algo:ComputingRS} by adding a certain condition, e.g. stated in \eqref{MinProg:DualCert} for $s=\sign{x^*_I}$, for regarding a specific values of $w$ after the while-loop starting in line \ref{Algo1:WhileLoop}.

% This may be interesting for exploring characteristics for different dual certificates.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "main"
%%% End: 
