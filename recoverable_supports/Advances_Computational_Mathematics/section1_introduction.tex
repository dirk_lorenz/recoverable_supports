\section{Introduction}
The difficulty of finding suitable test instances is a serious problem in the field of \textit{Sparse Reconstruction}.
A common and promising method to reconstruct a vector $x^*\in\mathbb{R}^n$ with only a few nonzeros entries from a linear transformation, which is realized by a matrix $A\in\mathbb{R}^{m\times n}$ with $m < n$, is performing the $\ell_1$ minimization, i.e. 
\begin{align}
x^*=\arg\min_y\|y\|_1\mbox{ s.t. } Ay=Ax^*.\label{l1}
\end{align}
This optimization problem was introduced in \cite{ChDoSa89} and is called \textit{Basis Pursuit}. 
Under certain conditions (e.g. see \cite{DoHu01,GrNi03,Tr05}) the vector $x^*$ is also a solution with the smallest number of nonzero entries; a vector $x^*$ with exactly $k$ nonzero entries is called \textit{$k$-sparse}.

A popular method for finding a $k$-sparse vector $x^*$ satisfying \eqref{l1} for a given matrix $A\in\mathbb{R}^{m\times n}$ is to choose an index set $I\subset\{1,\dots,n\}$ with cardinality $k$ and entries $x^*_i, i\in I$, randomly.
For small $k$ this procedure is promising especially if the conditions mentioned above are satisfied, but these conditions require small $k$.
For large $k$ it is more difficult to get suitable $k$-sparse vectors $x^*$.
Besides the question how to compute $x^*$ satisfying \eqref{l1} for a given matrix, we state the question how many different pairs of index sets and signums do exist for a given sparsity $k$.
We aim at partial answers to these questions in a non-asymptotic regime.

We denote by $I=\supp{x^*}$ the support of a vector $x^*$ and its complement by $I^c=\{1,\dots,n\}\backslash I$. 
By $A_I$ we denote the submatrix of a matrix $A$, whose columns are indexed by $I$, by $A_I^T$ its transpose, and set $s=\sign{x^*}_I$.
Further, the null space of $A$ is donted by $\ke{A}$ and the range of $A$ is denoted by $\rg{A} = \{Ax : x\in\mathbb{R}^n\}$.
For~(\ref{l1}) to hold, it is necessary and sufficient (cf.~\cite[Theorem 2]{Pl07}) that
\begin{align}
 \exists w\in\mathbb{R}^m : A_I^Tw=s, \|A_{I^c}^Tw\|_\infty<1\mbox{ and }A_I\mbox{ has full rank}.\label{Conditions:l1}
\end{align}
A vector $w$ fulfilling~(\ref{Conditions:l1}) will be called \emph{dual certificate for the support $I$ and sign $s$}.
Condition (\ref{Conditions:l1}) shows that the recoverability of the solution $x^*$ only depends on its support and its signum.
\begin{Definition}
Let $A\in\mathbb{R}^{m\times n}$ and $k\le m\le n$. For $I\subset\{1,\dots,n\}$ and $s\in\{-1,1\}^I$, a pair $(I,s)$ satisfying \eqref{Conditions:l1} is called \textit{Recoverable Support} of $A$. 
If $I$ has the cardinality $k$, a Recoverable Support $(I,s)$ has the \textit{size} $k$.
\end{Definition}

Thus, finding $x^*\in\mathbb{R}^n$ which satisfies \eqref{l1} for a given matrix $A\in\mathbb{R}^{m\times n}$ is equivalent to finding a corresponding Recoverable Support $(I,s)$ such that $I=\supp{x^*}$ and $s = \sign{x^*}_I$.
For the rest of this paper we will denote the cardinality of a set $I$ with $|I|$ and the $i$-th column of a matrix $A$ with $a_i$.
Moreover we will require $m\le n$ for all ${m\times n}$-matrices.

With a geometrical interpretation of \eqref{Conditions:l1}, new insights to Basis Pursuit, including what kind of matrices can be used and how many Recoverable Supports do exist for a certain size $k$, can be developed.
To that end, consider that $A^Tw$ is a relative interior point of an $(n-|I|)$-dimensional face of the $n$-dimensional hypercube $C^n := [-1,+1]^n$ and assume that the range of $A^T$ is an $m$-dimensional subspace.
Hence, condition \eqref{Conditions:l1} leads to the geometrical interpretation that the $m$-dimensional subspace $\rg{A^T}$ cuts the relative interior of an $(n-|I|)$-dimensional face of $C^n$.
In \cite{Pl07}, the resulting polytope emerging from the intersection of the $m$-dimensional subspace and $C^n$ is considered.
Counting all index sets $I\subset\{1,\dots,k\}$ with $|I|=k$, which satisfy this geometrical interpretation, one can give exact values for the numbers of recoverable vectors for a matrix $A$ and a sparsity $k$.
These values have been estimated in several papers (e.g. \cite{BrDoEl09,DoPeFa10,TsDo06}) through Monte Carlo samplings.
Further this interpretation brings \textit{Sparse Reconstruction} together with the topic \textit{(cross-)sections of a hypercube} in Combinatorial Geometry.

A different geometrical interpretation has been given by Donoho in \cite{Do04} through associating projected $n$-dimensional cross-polytopes with the Basis Pursuit problem, see also the accessible description in~\cite[Section 4.5]{FR13}.
The connection between \textit{Sparse Reconstruction} and the theory of \textit{convex polytopes} gave new insights in both fields. 
Our geometrical interpretation of Recoverable Supports, which is inspired by~\ref{Conditions:l1}, is dual to this approach.
Nonetheless our interpretation delivers additional insights to the questions posed above.

This paper is organized as follows.
In Section 2 we develop conditions for the existence of Recoverable Supports.
The geometrical aspect around the stated geometrical interpretation will be regarded more carefully in Section 3: a proof for the geometrical interpretation of Recoverable Supports will be given, and exact numbers of Recoverable Supports for certain types of matrices as well as a non-trivial upper bound for these numbers will be stated.
Further we will introduce an algorithm to compute a Recoverable Support of a given matrix and a given size in Section 4.
The theoretical results from these sections will be illustrated by Monte Carlo experiments in Section 5.
Through numercial experiments we additionally provide evidence that checking \eqref{Conditions:l1} is considerably faster than solving Basis Pursuit as a linear program.
In addition, our method stands out from recently done experiments since we can also ensure that a vector is the \emph{unique} solution of Basis Pursuit, without restricting the test problems to a certain class of matrices (e.g. random matrices).
% (e.g. \textit{full spark frames}).
% This is important for applications where a certain vector shall be recovered, as computed tomography.% Ggf. rein mit Referenz auf Sidky, Jörgensen et al.?


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "main"
%%% End: 
