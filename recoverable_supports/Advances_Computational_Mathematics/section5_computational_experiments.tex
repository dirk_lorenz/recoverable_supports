\section{Computational Experiments}
In this section, we present computational experiments for the topics of the previous sections.
The optimization problem \eqref{MinProg:DualCert} delivers an alternative method to perform numercial experiments in Basis Pursuit.
A comparison of solving \eqref{MinProg:DualCert} and solving the $\ell_1$ minimization in \eqref{l1} will be done in the following subsection.
In Subsection \ref{Se:NRSCM} we will highlight the theorectical results from Section 3 with Monte Carlo experiments and will show the behaviour of the heuristic upper bound from \eqref{InEq:UpBoundBeta}.
% Further we will give empirical results on Algorithm \ref{Algo:ComputingRS} in Subsection \ref{Se:PerformAlgo}.
% In the last subsection, we extend \eqref{l1} to the analysis $\ell_1$ minimization problem and present results similar to Subsection \ref{Se:Com_l1_l_inf} and \ref{Se:NRSCM}.

All experiments were done with Matlab R2012b employed on a desktop computer with 4 CPUs, each Intel\textregistered\ Core\texttrademark\ i5-750 with 2.67GHz, and 5.8 GB RAM; the $\ell_1$ and $\ell_\infty$ minimization problems were solved as linear programs with Mosek 6.

In the Monte Carlo experiments it will be tested whether a pair $(I,s)$, with $I\subset\{1,...,n\}, s\in\{-1,1\}^I$, is a Recoverable Support of a given matrix.
The experiments were done as follows:
For a given matrix $A\in\mathbb{R}^{m\times n}$ and $k\le m$, we generate $I\subset\{1,...,n\}$ with $|I|=k$ randomly by choosing $I$ uniformly at random over $\{1,...,n\}$ and assure whether the submatrix $A_I$ has full rank through the Matlab function \texttt{rank}.
If $A_I$ has no full rank, then $(I,s)$ is not a Recoverable Support of $A$; otherwise we also choose $s\in\{-1,1\}^I$ randomly and solve the $\ell_\infty$ minimization problem \eqref{MinProg:DualCert} with $s = \sign{x^*_I}$.
If the optimization problem is feasible, solved with status 'optimal' and its optimization value is strictly less than one, the pair $(I,s)$ will be recorded as a Recoverable Support of $A$.
For each size $k$, we perform $M$ repetitions and average the results; the number $M$ varies from experiment to experiment and may be obtained from the descriptions to each experiment.
For reproducibility the code for all tests is at \homepage.

\subsection{Comparing $\ell_1$ and $\ell_\infty$ Solver in Mosek}\label{Se:Com_l1_l_inf}

To check whether a pair $(I,s)$ is a Recoverable Support, there are different methods, e.g. outlined in Section~\ref{sec:comp-rec-supp}.
In this subsection, we compare the na\"ive approach, i.e. solving \eqref{Conditions:l1} for some $x^*$ with the desired signum $s$, with solving \eqref{MinProg:DualCert}.
For comparision, we decided to perform a similar setup as in typical studies of the \textit{Phase Transition}, see e.g. \cite{DoTa09}.
We chose, as in \cite{DoTa09}, Gaussian matrices $A\in\mathbb{R}^{m\times n}$ for fixed $n=1600$ and varying $m$ such that $\delta=m/n\in(0,1]$ is chosen in forty equidistant steps.
The tests were realized as Monte Carlo experiments with varying $|I|=k$ such that for any $m$ the value $\rho=k/m\in(0,1]$ is chosen in forty equidistant steps.
For any triple $(m,n,k)$, we did the following testing.
We chose $A\in\mathbb{R}^{m\times n}$ as a randomly drawn Gaussian matrix, and performed the Monte Carlo sampling as described above by firstly check whether $(I,s)$ is a Recoverable Support of $A$, then choose $x^*$ with $\supp{x^*}=I, \sign{x^*}_I = s$, and solve Basis Pursuit with the right-hand side $Ax^*$.
This procedure is done with $M=10$ repetitions.
Remarkably, both approaches can be cast as solutions of linear programs and hence, we used the same solver for linear programs.
More precisely, testing whether $(I,s)$ is a Recoverable Support by solving \eqref{MinProg:DualCert} was implemented as a linear program and solved with the Mosek routine \texttt{mosekopt} with all tolerances set to default.
We decide that the pair $(I,s)$ is a Recoverable Support if $A_I$ has full rank, the optimization problem is feasible, it is solved with a status 'Optimal', and its objective value is strictly less then $1-10^{-12}$.
On the other hand, we checked whether $x^*$ satisfies \eqref{l1} by solving the constrained $\ell_1$ minimization as a linear program with the Mosek routine \texttt{mosekopt}; again all tolerances were set to default.
We judge a calculated solution $\tilde x$ to be exact if $\|\tilde x-x^*\|<10^{-5}$.

First we observe that all calculated solutions were solved with the status ``Optimal''.
Figure \ref{Fig:Time_Comp_PT} displays the averaged results of the decision whether a calculated solution of $\ell_1$ minimization is the desired solution (left) and a tested pair is a Recoverable Support (right).
The miss-fit between the figures comes from the fact that the solutions of~(\ref{l1}) are not accurate enough to fulfill the desired tolerance of $10^{-5}$. %, even though the tolerances for the solver were set quite high.
Relaxing the bound from $10^{-5}$ to $10^{-3}$ would lead to almost identical figures in this case but may lead to more errors in other circumstances.
Alternatively, instead of measuring the Euclidean distance between the calculated solution $\tilde x$ and the actual solution $x^*$, one may compare whether the support of $x^*$ and the support of $\tilde x$ coincide; however, to determine the support, another tolerance would be needed to identify the nonzero entries.
In perspective to previous experiments, e.g. \cite{DoTa09}, the results as in Figure~\ref{Fig:Time_Comp_PT} are as expected.
Further, we see agreement to previous testings as the phase transition between one to zero is displayed by the curve $\rho_F$ from Theorem \ref{Th:PropNeighbWeak} (cf. Figure \ref{Fig:PhaseTransition}).

\begin{figure}
\begin{center}\includegraphics[width=60mm]{graphics/newgraphics/section4-time-compBP-PTL1.png}
\includegraphics[width=60mm]{graphics/newgraphics/section4-time-compBP-PTRS.png}\end{center}
\caption{Averaged results from Monte Carlo experiments whether test instance is solved by $\ell_1$ minimization (left) and \eqref{MinProg:DualCert} (right). The values reach from zero (none of the instances where solutions) to one (all instances were solutions).}
\label{Fig:Time_Comp_PT}
\end{figure}

For measuring the performance of both procedures, we measure the time it took to solve each linear program.
We excluded all operations to formulate the constraints of the linear programs from the time measurement.
Additionally before solving \eqref{MinProg:DualCert}, we checked whether $A_I$ has full rank and measure its duration.
If $A_I$ is not a full rank matrix, the problem \eqref{MinProg:DualCert} would not be solved.
Since we are only considering Gaussian random matrices, which are full spark matrices with probability 1, we could have skipped the testing of the rank (and we would have saved about 0.7 percent of the entire run time of the test) but we decided to present the test without any restrictions to specific test problems.

In dependence of $\delta$ and $\rho$, Figure \ref{Fig:Time_Comp} shows the averaged duration of solving \eqref{MinProg:DualCert} and calculating the rank of the submatrix divided by the averaged duration of the $\ell_1$ minimization.
One may observe that all quotients are less than one which means that in all cases solving \eqref{MinProg:DualCert} and checking the injectivity of the submatrix is faster than solving Basis Pursuit as a linear program.
Figure~\ref{Fig:Time_Comp_2} illustrates that the duration of both methods do increase with an increasing $\delta$, but while solving \eqref{MinProg:DualCert} seems to depend only on $\delta$, the $\ell_1$ minimization depends on $\delta$ and also on $\rho$.
Moreover, the contours of $\rho_F$ from Theorem \ref{Th:PropNeighbWeak} can be seen in the duration of time at the $\ell_1$ minimization as well as in the Figure \ref{Fig:Time_Comp_PT}: one may say that, on average, solving Basis Pursuit at $\rho=\rho_F(\delta)$ takes more time than solving it at any different $\rho$ in the neigborhood of $\rho_F(\delta)$.
Additionally, for small $\delta$ only small differences up to a quotient of $4/5$ appear in the comparision of the time duration.
In total, the use of checking~\eqref{MinProg:DualCert} instead of doing $\ell_1$ minimization reduces the computational time by a factor of $0.29$
(which amounts to a total save of 16 hours of computational time in our experiments).
% Indeed, most experiments  use a striktly higher number $M$ of repetitions, e.g. $M=200$ in \cite{DoTa09}, than we did.

\begin{figure}
\begin{center}\includegraphics[width=100mm]{graphics/Time_comp.pdf}\end{center}
\caption{Comparision of duration performances between $\ell_1$ minimization and \eqref{MinProg:DualCert} and checking injectivity.}
\label{Fig:Time_Comp}
\end{figure}

Furthermore, one may observe that the quotients decrease between $\delta = 0.225$ and $\delta=0.25$.
This phenomenon stems from the duration of the $\ell_1$ minimization program, cf. Figure~\ref{Fig:Time_Comp_2}.
We  believe that an internal change in Mosek, where it is decided whether the primal or the dual problem should be solved, causes this behaviour.

%Additionally, Figure \ref{Fig:Time_Comp_2} shows another interesting phenomenon: the contours of $\rho_F$ from Theorem \ref{Th:PropNeighbWeak} can be seen in the duration of time at the $\ell_1$ minimization but not in the one for checking~\eqref{MinProg:DualCert}: On average, one may say that solving Basis Pursuit at $\rho=\rho_F(\delta)$ takes more time than solving it at any different $\rho$ in the neigborhood of $\rho_F(\delta)$.
% None of these surprising phenomenons can be observed in at the duaration of solving \eqref{MinProg:DualCert} with injectivity check, cf. Figure~\ref{Fig:Time_Comp_2} (right).

%This is highlighted by the fact that for fixed $\delta$ the value $\rho_F(\delta)$ is mostly larger than the rest of the values $\rho$ in this column.
%That means that for the pair $(\delta,\rho_F(\delta))$ our approach is once again remarkably faster than $\ell_1$ minimization.

\begin{figure}
\begin{center}\includegraphics[width=60mm]{graphics/Time_comp_1.pdf}
\includegraphics[width=60mm]{graphics/Time_comp_2.pdf}\end{center}
\caption{Duration of $\ell_1$ minimization (left) and solving \eqref{MinProg:DualCert} with injectivity check (right) in seconds.}
\label{Fig:Time_Comp_2}
\end{figure}


\subsection{Number of Recoverable Supports for Certain Types of Matrices}\label{Se:NRSCM}
In this subsection, we compare computational experiments on the number of Recoverable Supports of several types of matrices with results from Section 3 whereas we restrict our experiments to minimally redundant matrices.
The computational experiments were done by Monte Carlo experiments described above.
Since in the previous sections only Gaussian matrices as well as Mercedes-Benz frames were considered, we will use these types as test problems.
Note that in any repetition of the Monte Carlo procedure, a new Gaussian matrix is drawn.
Further note that the calculated value approximates the expected number of Recoverable Suppports divided by the total number of different pairs $(I,s), I\subset\{1,...,n\}, |I|=k, s\in\{-1,+1\}^I$, as in~\eqref{Map:Recov}.

Similar to Section \ref{Se:Com_l1_l_inf}, the experiments were done by checking \eqref{Conditions:l1} through checking whether the corresponding submatrix is injective and solving \eqref{MinProg:DualCert} afterwards.
If the optimal value is strictly less than $1-10^{-12}$, we record the chosen pair $(I,s)$ as a Recoverable Support.
We did the experiments with $n = 15, 34, 155$ and $n= 555$ and all $|I|=k\le n-1$.
For each $k$ we did $M=1000$ repetitions.

In Figures \ref{Fig:N=15}-\ref{Fig:N=555} all results are shown averaged.
The size $k$ of the desired Recoverable Support is given on the x-axis, on the y-axis the probability of recoverability is shown in percent.
These functions are empirical approximations of the mapping \eqref{Map:Recov}.
For comparison, the heuristic upper bound from \eqref{InEq:UpBoundBeta} in proportion to the total number $2^k\binom{n}{k}$ is also displayed.
Additionally, a circle for each type of matrix denotes the size $k$ when the recoverability at $k+1$ is less then one hundred percent (\textit{Empirical Bound}).
The empirical bounds are upper bounds for the smallest value $k$ where the actual recoverability \eqref{Map:Recov} at $k+1$ is less than one hundred percent, since there exists one pair $(I,s)$ which is not a Recoverable Support and the recoverability curve \eqref{Map:Recov} is monotonically nonincreasing by Proposition \ref{Pr:Recov}.
Note that in almost all cases (e.g. $n = 155, k = 111$ in Figure~\ref{Fig:N=155}) the empirical recoverability curves are not monotonically nonincreasing due their empirical nature.
The black cross denotes the last $k$ for which the recoverability guarantee for small sizes in Corollary \ref{Co:MuCo} holds (\textit{Bound Mutual Coherence}).
All figures only show results from the smallest of all displayed bounds to $n-1$, since the tests deliver a recoverability of one hundred percent for the missing sizes.
Besides the empirical results for the Mercedes-Benz frame $A$, Figure \ref{Fig:N=15} shows the actual ratio $\Lambda(A,k)\left[2^k\binom{n}{k}\right]^{-1}$ in black with respect to $k$ for $n=15$.
For these results each of the $2^{|I|}\binom{15}{|I|}$ pairs $(I,s)$ with $|I|\le 14$ have been checked solving \eqref{MinProg:DualCert} if it was a Recoverable Support.

\begin{figure}
\begin{center}\includegraphics[width=100mm]{graphics/fig_5_n_15.pdf}
\end{center}
\caption{Monte Carlo Sampling for $n=15, m=14$. The black curve respresents the actual number of Recoverable Supports of the Mercedes-Benz frame proportional to $2^k\binom{15}{k}$.}
\label{Fig:N=15}
\end{figure}
\begin{figure}
\begin{center}\includegraphics[width=60mm]{graphics/fig_5_n_34.pdf}
\includegraphics[width=60mm]{graphics/fig_5_n_155.pdf}\end{center}
\caption{Monte Carlo Sampling for $n=34, m=33$ (left) and $n=155, m=154$ (right).}
\label{Fig:N=155}
\end{figure}
\begin{figure}
\begin{center}\includegraphics[width=60mm]{graphics/fig_5_n_555.pdf}
\includegraphics[width=60mm]{graphics/fig_5_n_555_2.pdf}\end{center}
\caption{Monte Carlo Sampling for $n=555, m=554$. Left: segment from the ``Empirical Bound Gaussian'' to $k=554$. Right: segment from the ``Empirical Bound Mercedes-Benz'' to $k=554$, this graphics is a segment of the left graphics.}
\label{Fig:N=555}
\end{figure}


We emphasize that Mosek solved all problems with the status 'Optimal'.
In Figure \ref{Fig:N=15} one can see for the Mercedes-Benz frame that the results of the Monte Carlo sampling (blue) coincide with the actual values (black) up to an error of $10^{-1}$.
We tolerate this margin of error since improving the precision on one-tenth, we need to increase the number of samplings $M$ a hundredfold.
All results are bounded by the Upper Bound (red) except for the Mercedes-Benz frame in this case, which obviously is owed by the lack of accuracy.
Further the ``Bound Mutual Coherence'' coincides with the empirical bound for the Mercedes-Benz frame, which is not the case in the other cases.
Only in the case $n=155$ the ``Bound Mutual Coherence'' is the weakest bound, but as expected the distance to the ``Empirical Bound Mercedes-Benz''' increases with increasing $n$.
In all cases, Mercedes-Benz has the largest empirical bound.
At $n=155$, this values is $k=151$, while for $n=555$ it is $k=543$.
However, the distance between the 'Empirical Bound Mercedes-Benz' and the Upper Bound reaching one hundred percent increases with increasing $n$.
Additionally, Proposition \ref{Pr:SparsityK-1} holds for all suitable cases except an error of at most $10^{-2}$.
Hence, the results underlay the expectation that \eqref{InEq:UpBoundBeta} is a good bound for $k$ close to $n-1$.

Regarding Gaussian matrices, we observe that these matrices do not exceed the empirical recoverability curve of the Mercedes-Benz frame if $n$ is odd.
Contrary, it is expected that, at least with $k$ close to $n-1$, the recoverability curves of the Gaussian matrices exceed the curve of the Mercedes-Benz frame in case $n$ even; this behaviour may be observed in Figure~\ref{Fig:N=155}.
%Furthermore a unique differentiation of the Mean and the Single Gaussian matrices can not be noticed:
%For several $n$ the empirical bounds switch their succession, e.g. in $n=155$ the 'Empirical Bound Mean Gaussian' is the smallest of both bounds, while in $n=555$ it is larger than the 'Empirical Bound Single Gaussian'.
%Even a tendency to the distance to the 'Bound Mutual Coherence' is unapparent.

As also observed in the past similar experiments (e.g. \cite{TsDo06,BrDoEl09}), in all cases one can notice a rapid transition from one hundred to zero percent as $k$ increases.

% \subsection{Performance of Algorithm \ref{Algo:ComputingRS}}\label{Se:PerformAlgo}
% \begin{figure}
% \begin{center}
% \includegraphics[width=64mm]{graphics/Alg_Time.jpg}
% \includegraphics[width=64mm]{graphics/Alg_Time_Iteration.jpg}
% \tiny
% \begin{tabular}{l|cccccccccccccccccccc}
% n = 155& & & & & & & && &&& & &&&&&&&\\
% k          & 76 & 77 & 78 & 79 &  81 & 91 & 101& 111& 131 & 151&152&153&154\\
% iterations & 75 & 78 & 76 & 154 &  153 & 148 & 143 & 138 & 128 & 118 & 113 &117 & 114\\
% \end{tabular}
% \end{center}
% \caption{Duration (left) and number of iterations (right) of Algorithm \ref{Algo:ComputingRS} for several sizes $k$ and $n=555$ and $n=155$.
% The time in the right graphics is presented in log-scale, the two right graphics are depicted in log-log scale.
% All x-axes display the size $k$ in proportion to the size of the matrix.
% The graphics in the right upper shows the results for Mercedes-Benz frame; the graphics in the right lower displays the results for Gaussian matrices.
% The table below presents selected values for the Mercedes-Benz frame with $n=155$.}
% \label{Fig:Alg_TimeIt}
% \end{figure}
% \begin{figure}
% \begin{center}\includegraphics[width=64mm]{graphics/Alg_Value_n154.jpg}
% \includegraphics[width=64mm]{graphics/Alg_Value_n554.jpg}
% \end{center}
% \caption{Value $\|A_{I^c}^Tw\|_\infty$ with $A$ being Gaussian matrix (blue) and Mercedes Benz frame (black) for $n=155$ (left) and $n=555$ (right) with their corresponding optimal value of \eqref{MinProg:DualCert}, MinProg-Values}.
% \label{Fig:AlgN=155_Norm}
% \end{figure}
% \noindent As introduced in Section 4, for a given matrix $A\in\mathbb{R}^{m\times n}$ and a given number $k\le m$, Algorithm \ref{Algo:ComputingRS} delivers a Recoverable Support $(I,s)$ of $A$ with size $k$ as well as a corresponding dual certificate $w\in\mathbb{R}^m$ satisfying \eqref{Conditions:l1}.
% In this subsection, we present results for Algorithm \ref{Algo:ComputingRS} with regard to duration, number of iterations and the value $\|A_{I^c}^Tw\|_\infty$. 
% We equip Algorithm \ref{Algo:ComputingRS} with a QR decompostion for computing the null space of the submatrix $A_I^T$ after line \ref{Algo1:ReSuSize1a} and a rank one update to QR decomposition in line \ref{Algo1:ChooseBasisVector}.
% Both will be realized with the standard Matlab routines \texttt{qr} respectively \texttt{qrinsert}.
% The real value $\lambda$ in line \ref{Algo1:MinProb} will be chosen by calculating all values satisfying $\|A_{I^c}^T(w+\lambda y)\|_\infty = 1$ and choosing the smallest $\lambda$ in absolute value.
% Further we modify Algorithm \ref{Algo:ComputingRS} in the sense that if the statement in line \ref{Algo1:IfCause} is false, a different $\lambda$ in line \ref{Algo1:MinProb} will be chosen for the same instance $(I,s)$.
% Geometrically, one may interpret this as choosing a different direction if the chosen does not lead to a Recoverable Support.
% Further routines as suggested in Section 4 were not integrated.
% From our previous experiences we have observed that, in general, the additional computational effort does not stand in proportion to the small number of \textit{bad} cases.
% For measuring the performance of Algorithm \ref{Algo:ComputingRS} we chose the duration of computing a Recoverable Support in seconds and the required number of iterations as performance indicators.
% Further for the calculated Recoverable Support $(I,s)$, we give the value $\|A_{I^c}^Tw\|_\infty$ with the proposed dual certificate $w$ by Algorithm \ref{Algo:ComputingRS} and compare this value to the optimal value of \eqref{MinProg:DualCert}, named \textit{MinProg-Value}.
% This comparision is done to give an impression what kind of dual certificates were calculated.
% The problem \eqref{MinProg:DualCert} is solved as a linear program in Mosek with \texttt{mosekopt}; all tolerances were set to default.

% Similar to the previous sections, we chose minimally redundant matrices as test problems.
% As before, the matrices $A\in\mathbb{R}^{(n-1)\times n}$ were chosen as Gaussian matrices (seed=25) and Mercedes-Benz frames, further we chose the sizes $n=155$ and $n=555$.
% For both types of matrices and any $k\le n-1$ we calculated a Recoverable Support with size $k$.

% In the following, we use $(I,s)$ and $w$ as a Recoverable Support and dual certificate proposed by Algorithm \ref{Algo:ComputingRS} respectively.
% Figures \ref{Fig:Alg_TimeIt} and \ref{Fig:AlgN=155_Norm} show the results from our testings.
% Besides the graphics, Figure \ref{Fig:Alg_TimeIt} includes a table with the results for selected values of the Mercedes-Benz frame which are chosen for revealing the tendency of the results.
% Since we will recognize similar behaviour in both cases for $n$, we decided to display only the case $n=115$ in the table.
% First we present the results for Gaussian matrices. 
% Algorithm \ref{Algo:ComputingRS} delivers for any $k\le n-1$ a Recoverable Support $(I,s)$ of the tested Gaussian matrices with size $k$ in $k-1$ iterations.
% The time with respect to $k$ is approximatively linear in both cases.
% Further $\|A_{I^c}^Tw\|_\infty$ is always less than $1-10^{-4}$ and in most cases recorded between $0.8$ and $1$, see Figure \ref{Fig:AlgN=155_Norm}.
% In opposite, for small $k$ the value \textit{MinProg-Value} is always close to zero; hence, both proposed quantities are on contrary sides of the scale.
% For $k$ close to $n-1$, \textit{MinProg-Value} is larger and finally coincides with $\|A_{I^c}^Tw\|_\infty$.
% This result is not surprising since we have experienced for a Maximal Recoverable Supports there only exists one dual certificate. 
% We conjecture that the left cross-section in Figure \ref{Fi:ExSection} depicts this case: for a given Maximal Recoverable Support the intersection of the corresponding edge of $C^n$ and the range of $A^T$ contains only one element.
% Further we do not notice any conspicuities, the results of the performance of Algorithm \ref{Algo:ComputingRS} are as we have expected.

% Unless the \textit{carefree} performances with Gaussian matrices, Algorithm \ref{Algo:ComputingRS} delivers interesting results with the Mercedes-Benz frame.
% Nevertheless, for any size Algorithm \ref{Algo:ComputingRS} delivers a Recoverable Support, but as depicted in the right upper graphics in Figure \ref{Fig:Alg_TimeIt}, for larger sizes, the algorithm has trouble finding a suitable path to a Recoverable Support with the desired size.
% With regards to the duration, for both $n$ values, the algorithm performs almost similar to the Gaussian case and, up to a certain size $k$, there are $k-1$ iterations needed.
% For $n=155$ the table in Figure \ref{Fig:Alg_TimeIt} emphasizes that after $k=76$ the algorithm has a quite higher number of iterations if $k$ is odd; the corresponding graphics shows for $k$ even that slightly less iterations than $k-1$ are needed.
% This behaviour may be seen after $k=340$ for $n=555$.
% In both cases, after these \textit{changing points}, several values of $\lambda$ in line \ref{Algo1:MinProb} and several basis elements in line \ref{Algo1:ChooseBasisVector} were tried to find a Recoverable Support.
% More precisely, the chosen $\lambda$ in line \ref{Algo1:MinProb} adds more than one new index to the index set: it happens that for $k$ odd, this addition is beyond the sparsity, hence, a new $\lambda$ has to be chosen; otherwise for $k$ even this addition does not corrupt the injectivity of the corresponding submatrix.
% The value $\|A_{I^c}^Tw\|_\infty$ is always less than $10^{-1}$.
% For $k\ge78$ the values $\|A_{I^c}^Tw\|_\infty$ alternates between zero ($k$ even) and a half ($k$ odd); for $k=76,77$ this value is close to one.
% In contrast, as we have expected, MinProg-Value is always zero for $k$ even.
% For $k$ odd the MinProg-Value increases for increasing $k$.
% For $n=555$ we observe similar results.

% Since the duration increases linearly with respect to the size $k$, and since a Recoverable Support is located for all sizes, we assess the results of this testing positively.
% Surprisingly, we did observe that Mercedes-Benz frames challenges the algorithm; we did not chose this test instance for that reason.
% But however, as we have expected, Algorithm \ref{Algo:ComputingRS} delivers a Recoverable Support of a given matrix with a desired size regardless of its difficulty.



% \subsection{Analysis $\ell_1$ Minimization}
% In this subsection, we transfer the previous results to the analysis $\ell_1$ minimization problem, i.e. for given $A\in\mathbb{R}^{m\times n}$ and $D\in\mathbb{R}^{n\times p}$ with $m\le n\le p$, we solve
% \begin{align}
% x^* = \arg\min_y\|D^Ty\|_1\mbox{ s.t. }Ay=Ax^*,\label{anal1}
% \end{align}
% with $x^*$ choosen such that $D^Tx^*$ has $l$ nonzero entries.
% Several papers as e.g. \cite{NaDaElGr12,VaPeDoFa13} cared about \eqref{anal1}.
% We continue these works by performing similar experiments as in Subsection \ref{Se:Com_l1_l_inf} and \ref{Se:NRSCM}: besides solving the underlying optimization probelm in \eqref{anal1} with an $\ell_1$ optimization problem in Mosek with the routine \texttt{mosekopt}, for $I=\supp{D^Tx^*}, |I|<p,$ we also check whether the condition
% \begin{align}
% \begin{array}{rl}
% \exists w\in\mathbb{R}^m, v\in\mathbb{R}^p: &A^Tw + Dv = 0,\\
% &-v_I = \sign{D_I^Tx^*}, \|v_{I^c}\|_\infty < 1,\\
% &\ker{A}\cap\ke{D_{I^c}^T}=\{0\}
% \end{array}\label{anal1:conditions1}
% \end{align}
% is satisfied.
% Note that if \eqref{anal1:conditions1} is satisfied then \eqref{anal1} holds.
% Beyond, we rewrite \eqref{anal1} in the form of \eqref{l1}, since the analysis $\ell_1$ minimization problem is equivalent to
% \begin{align}
% \min_z\|z\|_1\mbox{ s.t. }A(D^T)^\dagger z= Ax^*, z\in\rg{D^T},\label{anal1_2}
% \end{align}
% if $D$ has full row rank.
% The conditon \eqref{anal1:conditions1} has been implemended by using the Mosek routine \texttt{mosekopt}
% and solving
% \begin{align}
% \min_{(w,v)}\|v_{I^c}\|_\infty\mbox{ s.t. }-v_I = \sign{x^*}, A^Tw + Dv = 0,\label{MinProg:DualCertAna}
% \end{align}
% the problem \eqref{anal1_2} can be check by the conditions \eqref{Conditions:l1} with the same routine solving \eqref{MinProg:DualCert} from Section \ref{Se:Com_l1_l_inf} and \ref{Se:NRSCM} with the matrix $[A(D^T)^\dagger; I_d - D^T(D^T)^\dagger]$, whereby $I_d$ stands for the identity matrix, and the right-hand side $b=[Ax^*;0]$.

% Similar to our previous experiments, we measure the time it takes to solve each optimization problem, and the recoverability.
% Despite we have decided to neglect the computation effort of building the optimization problems, we additionally measure the time it takes to build $A(D^T)^\dagger$ in case \eqref{anal1_2}.

% The testing has been done in the following steps: first for given $A\in\mathbb{R}^{m\times n}, D\in\mathbb{R}^{n\times p}$ and $p-n<l< p$, we choose $I\subset\{1,...,p\}, |I|=l,$ randomly and, with $y\in\mathbb{R}^p$ containing normally distributed entries, we set $x^* = (I_d - D_{I^c}(D_{I^c}^T D_{I^c})^{-1} D_{I^c}^T)y$ as proposed in \cite[Section 2.2]{NaDaElGr12}.
% For $b=Ax^*$ the underlying problem in \eqref{anal1} will be solved by Mosek as a linear program; if for the caluclated solution $\tilde x$ holds $\|\tilde x-x^*\|_2\le 10^{-5}$, we record the test as a success otherwise as a failure.
% Next, we check $\ker{A}\cap\ke{D_{I^c}^T}=\{0\}$ by considering the cardinality of intersections. 
% If this statement is true, the problem \eqref{MinProg:DualCertAna} will be solved; we record this test as a success if the optimal value of \eqref{MinProg:DualCertAna} is strictly less then $1-10^{-12}$.
% Equally, we solve \eqref{anal1_2} with the conditions \eqref{Conditions:l1} as described above.

% For the tests comparable to Subsection \ref{Se:NRSCM}, we also check the condition established in \cite[Theorem 2]{VaPeDoFa13} and compare it to the recently posed conditions. 
% Vaiter et.al. prove if $\ker{A}\cap\ke{D_{I^c}^T}=\{0\}$ and for an explicit matrix $\Phi$ (cf. \cite[Definition 4 and 3]{VaPeDoFa13} the optimal value of
% \[\min_{u\in\ker{D}}\|\Phi\sign{D_I^Tx^*}-u\|_\infty\]
% is strictly less than one, then \eqref{anal1} holds.
% As proposed in \cite[Section IV B]{VaPeDoFa13}, we solved it with the Douglas-Rachford splitting algorithm \cite{CoPe07} and record this test as a success if the optimal value is strictly less than $1-10^{-12}$.

% Similary as in the experiments in \cite{NaDaElGr12}, we have chosen $D^T$ as a random tight frame with unit norm colums, the construction can be found in \cite[Section 6.1]{NaDaElGr12}, and as the one-dimensional forward-difference matrix.
% The matrix $A$ is chosen as a Gaussian matrix. 
%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "main"
%%% End: 
